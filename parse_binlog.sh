#!/bin/bash
###################################################################
# name:         binlog_parse.sh
# version:      v1.0
# createBy:     libin
# createDate:   2023-09-06
# releaseNote
#    - 2023-09-06 v1.0 初始版本
# 			 - 通过测试版本: MySQL8.0.28、MySQL5.7.30
#        - 前置条件: row模式、binlog版本为v4、开启GTID
###################################################################

BINLOG_FILE_NAME=$1
TRANS_NUM=$2
MYSQL_BIN_DIR='/data/mysql/3306/base/bin'

logWrite()
{
  echo -e "`date +'%Y-%m-%d %H:%M:%S'` \033[32m[INFO] ${1} \033[0m"
}

logWriteWarning()
{
  echo -e "`date +'%Y-%m-%d %H:%M:%S'` \033[33m[WARNING] ${1} \033[0m"
}

logWriteError()
{
  echo -e "`date +'%Y-%m-%d %H:%M:%S'` \033[31m[ERROR] ${1} \033[0m"
  exit 1
}

if [ $# != 2 ] ; then
    logWriteError "Usage: ./$(basename $0) binlogname trans_num"
fi

# 同名临时文件防止覆盖
for file in binlog_init.tmp binlog_parse.tmp binlog_gtid.tmp; do
  if [ -f "$file" ]; then
    logWriteError "$file exists."
  fi
done

logWriteWarning "开始解析BINLOG: ${BINLOG_FILE_NAME}"

# 获取前TRANS_NUM个大事务
${MYSQL_BIN_DIR}/mysqlbinlog ${BINLOG_FILE_NAME} | grep "GTID$(printf '\t')last_committed" -B 1  | grep -E '^# at' | awk '{print $3}' | awk 'NR==1 {tmp=$1} NR>1 {print ($1-tmp,tmp);tmp=$1}' | sort -n -r -k 1 | head -n ${TRANS_NUM} > binlog_init.tmp

while read line
do
    TRANS_SIZE=$(echo ${line} | awk '{print $1}')
    logWriteWarning "TRANS_SIZE: $(echo | awk -v TRANS_SIZE=${TRANS_SIZE} '{ print (TRANS_SIZE/1024/1024) }')MB"
    FLAG_POS=$(echo ${line} | awk '{print $2}')
    # 获取GTID
    ${MYSQL_BIN_DIR}/mysqlbinlog -vvv --base64-output=decode-rows ${BINLOG_FILE_NAME} | grep -m 1 -A3 -Ei "^# at ${FLAG_POS}" > binlog_parse.tmp
    GTID=$(cat binlog_parse.tmp | grep -i 'SESSION.GTID_NEXT' | awk -F "'" '{print $2}')
    # 解析出GTID对应的binlog
    ${MYSQL_BIN_DIR}/mysqlbinlog --base64-output=decode-rows -vvv --include-gtids="${GTID}" ${BINLOG_FILE_NAME} > binlog_gtid.tmp
    START_TIME=$(grep -Ei '^BEGIN' -m 1 -A 3 binlog_gtid.tmp | grep -i 'server id' | awk '{print $1,$2}' | sed 's/#//g')
    END_TIME=$(grep -Ei '^COMMIT' -m 1 -B 1 binlog_gtid.tmp | head -1 | awk '{print $1,$2}' | sed 's/#//g')
    TRANS_START_POS=$(grep -Ei 'SESSION.GTID_NEXT' -m 1 -A 1 binlog_gtid.tmp | tail -1 | awk '{print $3}')
    TRANS_END_POS=$(grep -Ei '^COMMIT' -m 1 -B 1 binlog_gtid.tmp | head -1 | awk '{print $7}')
    # 输出
    logWrite "GTID: ${GTID}"
    logWrite "START_TIME: $(date -d "${START_TIME}" '+%F %T')"
    logWrite "END_TIME: $(date -d "${END_TIME}" '+%F %T')"
    logWrite "TRANS_START_POS: ${TRANS_START_POS}"
    logWrite "TRANS_END_POS: ${TRANS_END_POS}"
    # 统计对应的DML语句数量
    logWrite "该事务的DML语句及相关表统计:"
    grep -Ei '^### insert' binlog_gtid.tmp | sort | uniq -c
    grep -Ei '^### delete' binlog_gtid.tmp | sort | uniq -c
    grep -Ei '^### update' binlog_gtid.tmp | sort | uniq -c

done < binlog_init.tmp

rm -rf binlog_init.tmp
rm -rf binlog_parse.tmp
rm -rf binlog_gtid.tmp
